let http = require("http");

http.createServer(function (request, response) {

	if(request.url == "/items" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data retrieved from the database');
	}

	if(request.url == "/items" && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data to be sent to the database');
	}

}).listen(4000);

console.log('Server running at localhost:4000');




// 1. In the s32 folder, create an activity file inside of it.

// 2. Create a simple server and the following routes with their corresponding HTTP methods and responses:
//  - If the url is http://localhost:4000/, send a response Welcome to Booking System
//  - If the url is http://localhost:4000/profile, send a response Welcome to your profile!
//  - If the url is http://localhost:4000/courses, send a response Here’s our courses available
//  - If the url is http://localhost:4000/addcourse, send a response Add a course to our resources
const express = require('express');
const app = express();
const port = 4000;

app.get('/', (req, res) => {
  res.send('Welcome to Booking System');
});

app.get('/profile', (req, res) => {
  res.send('Welcome to your profile!');
});

app.get('/courses', (req, res) => {
  res.send('Here’s our courses available');
});

app.get('/addcourse', (req, res) => {
  res.send('Add a course to our resources');
});

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
// 3. Create a simple server and the following routes with their corresponding HTTP methods and responses:
//  - If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources
//  - If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources
// Test all the endpoints in Postman.
const express = require('express');
const app = express();
const port = 4000;

app.get('/', (req, res) => {
  res.send('Welcome to Booking System');
});

app.get('/profile', (req, res) => {
  res.send('Welcome to your profile!');
});

app.get('/courses', (req, res) => {
  res.send('Here’s our courses available');
});

app.get('/addcourse', (req, res) => {
  res.send('Add a course to our resources');
});

app.get('/updatecourse', (req, res) => {
  res.send('Update a course to our resources');
});

app.get('/archivecourses', (req, res) => {
  res.send('Archive courses to our resources');
});

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
// 4. Create a gitlab project repository named activity in s32 subgroup.

// 5. Initialize a local git repository, add the remote link and push to git with the commit message of Add s32 activity code.

// 6.     Add the link in Boodle.